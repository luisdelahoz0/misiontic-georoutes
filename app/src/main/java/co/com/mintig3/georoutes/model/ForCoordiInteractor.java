package co.com.mintig3.georoutes.model;

import java.util.ArrayList;
import java.util.List;

import co.com.mintig3.georoutes.mpv.ForCoordiMVP;
import co.com.mintig3.georoutes.view.dto.ForCoordiItem;

public class ForCoordiInteractor implements ForCoordiMVP.Model {

    //Se crea el item de forma temporal
    private List<ForCoordiItem> tempItems;

    public ForCoordiInteractor() {
        tempItems = new ArrayList<>();
        //tempItems.add(new ForCoordiItem("Save the coordinates", "November 20,2021"));
    }

    @Override
    //Esto por el momento esta almacenado en memoria y no esta conectado a ninguna base de datos
    public List<ForCoordiItem> getCoordinates() {
        return new ArrayList<>(tempItems);
    }

    @Override
    public void saveCoordinate(ForCoordiItem coordinate) {
        tempItems.add(coordinate);
    }

    @Override
    public void deleteCoordinate(ForCoordiItem coordinate) {

    }
}
