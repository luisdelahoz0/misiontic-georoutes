package co.com.mintig3.georoutes.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.com.mintig3.georoutes.model.database.entity.User;
import co.com.mintig3.georoutes.model.repository.FirebaseAuthRepository;
import co.com.mintig3.georoutes.model.repository.UserRepository;
import co.com.mintig3.georoutes.mpv.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private final FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context) {
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override//if override means that the method is public
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        Log.i(LoginInteractor.class.getSimpleName(), "Sesión iniciada exitosamente");
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        Log.i(LoginInteractor.class.getSimpleName(), "No se ha iniciado sesión");
                        callback.onFailure();
                    }
                });

    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleIntent() {
        return firebaseAuthRepository.getGoogleSignInIntent();
    }

    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.setGoogleData(data, new FirebaseAuthRepository.FirebaseAuthCallback() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }
}
