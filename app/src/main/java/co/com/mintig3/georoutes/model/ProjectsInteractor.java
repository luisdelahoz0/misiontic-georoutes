package co.com.mintig3.georoutes.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.mintig3.georoutes.model.database.entity.Project;
import co.com.mintig3.georoutes.model.repository.ProjectRepository;
import co.com.mintig3.georoutes.model.repository.UserRepository;
import co.com.mintig3.georoutes.mpv.LoginMVP;
import co.com.mintig3.georoutes.mpv.ProjectsMVP;

public class ProjectsInteractor implements ProjectsMVP.Model {

    private final ProjectRepository projectRepository;

    public ProjectsInteractor(Context context, String user) {
        projectRepository = new ProjectRepository(context, user);
    }

    @Override
    public void loadProjects(String user, LoadProjectsCallback<List<ProjectsMVP.ProjectDto>> callback) {
        projectRepository.getAllProjectsByUser(user, new ProjectRepository.ProjectsCallback<List<Project>>() {
            @Override
            public void onSuccess(List<Project> values) {
                List<ProjectsMVP.ProjectDto> projects = new ArrayList<>();
                for (Project project : values) {
                    projects.add(new ProjectsMVP.ProjectDto(project.getName(), project.getAddress()));
                }
                callback.onSuccess(projects);
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }
}
