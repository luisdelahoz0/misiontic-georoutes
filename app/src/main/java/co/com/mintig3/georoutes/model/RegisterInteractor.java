package co.com.mintig3.georoutes.model;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import co.com.mintig3.georoutes.model.database.entity.User;
import co.com.mintig3.georoutes.model.repository.FirebaseAuthRepository;
import co.com.mintig3.georoutes.model.repository.UserRepository;
import co.com.mintig3.georoutes.mpv.RegisterMVP;

public class RegisterInteractor implements RegisterMVP.Model {
    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    public RegisterInteractor(Context context) {
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override
    public void createUser(User user, CreateUserCallback callback) {
        firebaseAuthRepository.createUser(user.getEmail(), user.getPassword(),
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        user.setUid(firebaseAuthRepository.getCurrentUser().getUid());
                        userRepository.createUser(user, new UserRepository.GetUserByEmailCallback<Void>() {
                            @Override
                            public void onSuccess(Void node) {
                                // send data toward the presenter
                            }

                            @Override
                            public void onFailure() {
                                // send data toward the presenter
                            }
                        });
                        Log.i(LoginInteractor.class.getSimpleName(), "Usuario creado exitosamente");
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        Log.i(LoginInteractor.class.getSimpleName(), "Error en la creación de usuario");
                        callback.onFailure();
                    }
                });
    }
}
