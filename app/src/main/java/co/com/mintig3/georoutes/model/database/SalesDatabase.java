package co.com.mintig3.georoutes.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import co.com.mintig3.georoutes.model.database.dao.ProjectDao;
import co.com.mintig3.georoutes.model.database.dao.UserDao;
import co.com.mintig3.georoutes.model.database.entity.Project;
import co.com.mintig3.georoutes.model.database.entity.User;

// para cargar la base de datos
@Database(entities = {User.class, Project.class}, version = 1)
public abstract class SalesDatabase extends RoomDatabase {
    private volatile static SalesDatabase instance;

    public static SalesDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), SalesDatabase.class, "sales-database")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract UserDao getUserDao();

    public abstract ProjectDao getProjectDao();
}
