package co.com.mintig3.georoutes.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import co.com.mintig3.georoutes.model.database.entity.Project;


@Dao
public interface ProjectDao {

    @Query("SELECT * FROM project")
    List<Project> getAll();

    @Query("SELECT * FROM project WHERE name = :name")
    Project getProjectByName(String name);

    @Query("SELECT * FROM project WHERE user = :user")
    List<Project> getAllProjectsByUser(String user);

    @Insert
    void insert(Project project);

    @Update
    void update(Project project);

    @Delete
    void delete(Project project);
}
