package co.com.mintig3.georoutes.model.repository;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import co.com.mintig3.georoutes.R;

public class FirebaseAuthRepository {
    // Creating a singleton (with the word static)
    private static FirebaseAuthRepository instance;
    private FirebaseUser currentUser;
    private final FirebaseAuth mAuth;
    private final GoogleSignInClient googleSignInClient;

    //singleton
    public static FirebaseAuthRepository getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseAuthRepository(context);
        }
        return instance;
    }

    private FirebaseAuthRepository(Context context) {
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(context, gso);
    }

    public boolean isAuthenticated() {
        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null) {
            currentUser = mAuth.getCurrentUser();
        }
        return currentUser;
    }

    public void logOut() {
        if (currentUser != null) {
            mAuth.signOut();
            currentUser = null;
        }
    }

    public void createUser(String email, String password, FirebaseAuthCallback callback) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // TODO creó el usuario correctamente
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        // TODO No se pudo crear el usuario.
                        callback.onFailure();
                    }
                });
    }

    public void validateUser(String email, String password, FirebaseAuthRepository.FirebaseAuthCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        // If sign in fails, display a message to the user.
                        callback.onFailure();
                    }
                });
    }

    public void setGoogleData(Intent data, FirebaseAuthCallback callback) {

        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            firebaseAuthWithGoogle(account.getIdToken(), callback);
        } catch (ApiException e) {
            callback.onFailure();
        }
    }

    private void firebaseAuthWithGoogle(String idToken, FirebaseAuthCallback callback) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((OnCompleteListener<AuthResult>) task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public interface FirebaseAuthCallback {
        void onSuccess();

        void onFailure();
    }

    //Google Auth
    public Intent getGoogleSignInIntent() {
        return googleSignInClient.getSignInIntent();
    }
}
