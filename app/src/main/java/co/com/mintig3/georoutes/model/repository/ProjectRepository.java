package co.com.mintig3.georoutes.model.repository;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import co.com.mintig3.georoutes.model.database.SalesDatabase;
import co.com.mintig3.georoutes.model.database.dao.ProjectDao;
import co.com.mintig3.georoutes.model.database.entity.Project;

public class ProjectRepository {

    private final ProjectDao projectDao;
    private final DatabaseReference projectRef;

    private final Boolean inDB = false;

    public ProjectRepository(Context context, String user) {

        this.projectDao = SalesDatabase.getInstance(context)
                .getProjectDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String path = "projects/" + user;
        projectRef = database.getReference(path);

    }

    public void getAllProjectsByUser(String user, ProjectsCallback<List<Project>> callback) {
        if (inDB) {
            callback.onSuccess(projectDao.getAllProjectsByUser(user));
        } else {
            projectRef
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<Project> projects = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            projects.add(snapshot.getValue(Project.class));
                        }
                        callback.onSuccess(projects);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        callback.onFailure();
                    }
                });
        }
    }

    public static interface ProjectsCallback<T> {
        void onSuccess(T value);

        void onFailure();
    }
}
