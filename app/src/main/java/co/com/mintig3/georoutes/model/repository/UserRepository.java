package co.com.mintig3.georoutes.model.repository;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import co.com.mintig3.georoutes.model.database.SalesDatabase;
import co.com.mintig3.georoutes.model.database.dao.UserDao;
import co.com.mintig3.georoutes.model.database.entity.User;

public class UserRepository {
    private final UserDao userDao;
    private final DatabaseReference userRef;

    private final Boolean inDB = false;

    public UserRepository(Context context) {
        this.userDao = SalesDatabase.getInstance(context)
                .getUserDao();

        //pruebaFirebase();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users");

        //loadInitialUsers();
    }

    private void pruebaFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(UserRepository.class.getSimpleName(), "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(UserRepository.class.getSimpleName(), "Failed to read value.", error.toException());
            }
        });
    }

    private void loadInitialUsers() {
        if (inDB) {
            //usar room database
            userDao.insert(new User("Cesar Diaz", "cdiaz@email.com", "12345678"));
            userDao.insert(new User("User Test", "test@email.com", "87654321"));
        } else {
            //usar firebase
            userRef.child("cdiaz_email_com").child("name").setValue("Cesar Diaz");
            userRef.child("cdiaz_email_com").child("email").setValue("example@email.com");
            userRef.child("cdiaz_email_com").child("password").setValue("12345678");

            User user = new User("User Test", "test@email.com", "87654321");
            userRef.child(getEmailId(user.getEmail())).setValue(user);
        }
    }

    public void getUserByEmail(String email, GetUserByEmailCallback callback) {
        if (inDB) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            userRef.child(getEmailId(email))
                .addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       // si encuentra un usuario devuélvalo como un objeto de esa clase
                       User user = dataSnapshot.getValue(User.class);
                       callback.onSuccess(user);
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {
                       callback.onFailure();
                   }
               });
        }
    }

    //un callback que no retorna nada "void"
    public void createUser(User user, GetUserByEmailCallback<Void> callback) {
        if(inDB) {
            callback.onSuccess(null);
        } else {
            User userToSave = new User(user.getName(), user.getEmail(), null);
            userRef.child(user.getUid())
                    .setValue(userToSave);
            callback.onSuccess(null);
        }
    }

    private String getEmailId(String email) {
        return email.replace('@', '_').replace('.', '_');
    }

    public static interface GetUserByEmailCallback<T> {
        void onSuccess(T node);

        void onFailure();
    }
}
