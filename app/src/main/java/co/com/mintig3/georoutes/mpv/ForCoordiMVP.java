package co.com.mintig3.georoutes.mpv;

import java.util.List;

import co.com.mintig3.georoutes.view.dto.ForCoordiItem;

public interface ForCoordiMVP {
    interface Model {

        List<ForCoordiItem> getCoordinates();

        void saveCoordinate(ForCoordiItem coordinate);

        void deleteCoordinate(ForCoordiItem coordinate);
    }

    interface Presenter {
        //Para el boton guardar cordenas y mostrar siguiente activity
        void onBtnSaveCoordinates();

        //Para el boton de + que se encuentra en el texInputLayout
        void addNewCoordinate();

        //Cargar las coordenadas
        void loadCoordinate();

        void ForCoordinateItemLongClicked(ForCoordiItem coordinate);

        void deleteCoordinate(ForCoordiItem coordinate);
    }

    interface View {

        ForCoordinatesInfo getForCoordinatesInfo();

        void showNamePolygonError(String error);

        void showDescriptionError(String error);

        void showAddCoordinatesError(String error);

        void showPolygonActivity();

        void showGenerarError(String error);

        //Para metodo para mostrar la vista de los item
        void showCoordinateList(List<ForCoordiItem> items);

        String getCoordinateDescription();

        void addCoordinateToList(ForCoordiItem coordinate);

        void showDeleteDialog(String message, ForCoordiItem coordinate);

        void deleteCoordinate(ForCoordiItem coordinate);
    }

    class ForCoordinatesInfo {
        private String namePolygon;
        private String description;
        private String addCoordinates;

        public ForCoordinatesInfo(String namePolygon, String description, String addCoordinates) {
            this.namePolygon = namePolygon;
            this.description = description;

        }

        public String getNamePolygon() {
            return namePolygon;
        }

        public String getDescription() {
            return description;
        }

        //public String getAddCoordinates() { return addCoordinates; }
    }
}
