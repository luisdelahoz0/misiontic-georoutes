package co.com.mintig3.georoutes.mpv;

import android.app.Activity;
import android.content.Intent;

public interface LoginMVP {

    interface Model {

        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);

        boolean hasAuthenticatedUser();

        Intent getGoogleIntent();

        void setGoogleData(Intent data, ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {
        void onLoginClick();

        void onGoogleClick();

        void isLoggedUser();

        void validateHasUserAuthenticated();

        void setGoogleData(Intent data);
    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();

        void showEmailError(String error);

        void showPasswordError(String error);

        void showSomeActivity(); //redirect to the main ProjectsActivity

        void showGeneralError(String error);

        void hideProgressBar();

        void showProgressBar();

        void showGoogleSingInActivity(Intent intent);
    }

    class LoginInfo {
        private String email;
        private String password;

        // alt+insert to obtain setters and getters
        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
