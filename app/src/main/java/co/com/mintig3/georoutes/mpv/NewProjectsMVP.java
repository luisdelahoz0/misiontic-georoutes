package co.com.mintig3.georoutes.mpv;

public interface NewProjectsMVP {
    interface Model {

    }

    interface Presenter {
        //btn New project add y save
        void onSaveNewClick();

    }

    interface View {
        NewProjectsInfo getNewProjectsInfo();

        void showNameProjectsError(String error);

        void showDescriptionProjectsError(String error);


        void showProjectPoligMenuActivity();

        void showGenerarError(String error);
    }

    class NewProjectsInfo {
        private String nameProject;
        private String descriptionProjects;

        public NewProjectsInfo(String nameProject, String descriptionProjects) {
            this.nameProject = nameProject;
            this.descriptionProjects = descriptionProjects;
        }

        public String getNameProject() {
            return nameProject;
        }

        public String getDescriptionProjects() {
            return descriptionProjects;
        }
    }
}
