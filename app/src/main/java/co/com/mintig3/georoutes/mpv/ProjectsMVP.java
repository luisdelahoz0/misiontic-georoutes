package co.com.mintig3.georoutes.mpv;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

import co.com.mintig3.georoutes.model.database.entity.Project;

public interface ProjectsMVP {
    
    interface Model {
        void loadProjects(String email, LoadProjectsCallback<List<ProjectDto>> callback);

        interface LoadProjectsCallback<T> {

            void onSuccess(T data);

            void onFailure();
        }
    }

    interface Presenter {
        void loadProjects();

        void onProjectsClick();

        void onBtnNewProjectClick();

        void onselectItem(ProjectDto item);

        String getUser();

        void logOut();
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showProjects(List<ProjectDto> projects);

        void openDetailsActivity(Bundle params);

        void showNewProjectsActivity();
    }

    class ProjectDto {
        private String project;
        private String address;

        public ProjectDto(String project, String address) {
            this.project = project;
            this.address = address;
        }

        public String getProject() {
            return project;
        }

        public String getAddress() {
            return address;
        }
    }
}
