package co.com.mintig3.georoutes.mpv;

import android.app.Activity;

import co.com.mintig3.georoutes.model.database.entity.User;

public interface RegisterMVP {
    interface Model {

        void createUser(User user, RegisterMVP.Model.CreateUserCallback callback);

        interface CreateUserCallback {
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {
        void onRegisterClick();

    }

    interface View {
        Activity getActivity();

        RegisterMVP.RegisterInfo getRegisterInfo();

        void showNameError(String error);

        void showEmailError(String error);

        void showPasswordError(String error);

        void showConfirmPasswordError(String error);

        void showPasswordsDontMatch(String error);

        void showSomeActivity(); //TODO redirect to the main ProjectsActivity

        void showGeneralError(String error);

        void hideProgressBar();

        void showProgressBar();
    }

    class RegisterInfo {
        private String name;
        private String email;
        private String password;
        private String confirmedPassword;

        // alt+insert to obtain setters and getters
        public RegisterInfo(String name, String email, String password, String confirmedPassword) {
            this.name = name;
            this.email = email;
            this.password = password;
            this.confirmedPassword = confirmedPassword;
        }

        public String getConfirmedPassword() {
            return confirmedPassword;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getName() {
            return name;
        }
    }

}
