package co.com.mintig3.georoutes.presenter;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import co.com.mintig3.georoutes.model.ForCoordiInteractor;
import co.com.mintig3.georoutes.mpv.ForCoordiMVP;
import co.com.mintig3.georoutes.view.dto.ForCoordiItem;
import co.com.mintig3.georoutes.view.dto.ForCoordiState;

public class ForCoordiPresenter implements ForCoordiMVP.Presenter {
    private ForCoordiMVP.View view;
    private ForCoordiMVP.Model model;

    public ForCoordiPresenter(ForCoordiMVP.View view){
        this.view = view;
        this.model = new ForCoordiInteractor();
    }

    @Override
    //Click en este btn
    public void onBtnSaveCoordinates() {
        ForCoordiMVP.ForCoordinatesInfo forCoordinatesInfo = view.getForCoordinatesInfo();

        //Valido datos
        boolean error = false;
        view.showNamePolygonError("");
        view.showDescriptionError("");
        //view.showAddCoordinatesError("");
        if (forCoordinatesInfo.getNamePolygon().isEmpty()){
            view.showNamePolygonError("Nombre de polígono requerido");
            error = true;
        }
        if (forCoordinatesInfo.getDescription().isEmpty()){
            view.showDescriptionError("Descripción requerida");
            error = true;

        }
        //if (forCoordinatesInfo.getAddCoordinates().isEmpty()){
          //  view.showAddCoordinatesError("Coordenadas requerida");
            //error = true;
        //}
        if (!error) {
            view.showPolygonActivity();
        } else {
        view.showGenerarError("Credenciales Inválidas");
    }

    }

    @Override
    public void addNewCoordinate() {
        //#2 Para agregar una nueva coordenada a la vista
        Log.i(ForCoordiPresenter.class.getSimpleName(), "Add new coordinate");
        String description = view.getCoordinateDescription();
        String date = SimpleDateFormat.getDateTimeInstance().format(new Date());

        ForCoordiItem coordinate = new ForCoordiItem(description, date);
        model.saveCoordinate(coordinate);

        view.addCoordinateToList(coordinate);

    }

    @Override
    //#1 Para cargar coordenadas para decirle al modelo que me entregue las coordenadas
    public void loadCoordinate() {
        List<ForCoordiItem> items = model.getCoordinates();

        view.showCoordinateList(items);
    }

    @Override
    public void ForCoordinateItemLongClicked(ForCoordiItem coordinate) {
        view.showDeleteDialog("¿Quieres eliminar la coordenada?", coordinate);
    }

    @Override
    public void deleteCoordinate(ForCoordiItem coordinate) {
        model.deleteCoordinate(coordinate);
        view.deleteCoordinate(coordinate);
    }
}
