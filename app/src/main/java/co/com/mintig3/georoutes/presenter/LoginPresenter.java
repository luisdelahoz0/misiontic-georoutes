package co.com.mintig3.georoutes.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import co.com.mintig3.georoutes.model.LoginInteractor;
import co.com.mintig3.georoutes.mpv.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter, LoginMVP.Model.ValidateCredentialsCallback {

    private final LoginMVP.View view;
    private final LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        // Validate data
        view.showEmailError("");
        view.showPasswordError("");
        if (loginInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("La contraseña no es válida");
            error = true;
        }
        if (!error) {
            view.showProgressBar();
            new Thread(() -> model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                    LoginPresenter.this)).start();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.endsWith(".com");
    }

    @Override
    public void onGoogleClick() {
        Intent intent = model.getGoogleIntent();
        view.showGoogleSingInActivity(intent);
    }

    @Override
    public void isLoggedUser() {
        SharedPreferences preferences = view.getActivity()
                .getSharedPreferences("auth", Context.MODE_PRIVATE);
        boolean isLogged = preferences.getBoolean("logged", false);
        if(isLogged) {
            view.showSomeActivity();
        }
    }

    @Override
    public void validateHasUserAuthenticated() {
        if(model.hasAuthenticatedUser()) {
            view.showSomeActivity();
        }
    }

    @Override
    public void setGoogleData(Intent data) {
        model.setGoogleData(data, new LoginMVP.Model.ValidateCredentialsCallback() {
            @Override
            public void onSuccess() {
                view.getActivity().runOnUiThread(() -> {
                    SharedPreferences preferences1 = view.getActivity()
                            .getSharedPreferences("auth", Context.MODE_PRIVATE);
                    preferences1.edit()
                            .putBoolean("logged", true)
                            .apply();

                    LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
                    SharedPreferences preferences2 = view.getActivity()
                            .getSharedPreferences("user", Context.MODE_PRIVATE);
                    preferences2.edit()
                            .putString("emailId", getEmailId(loginInfo.getEmail()))
                            .apply();
                    view.showSomeActivity();
                    view.hideProgressBar();
                });
            }

            @Override
            public void onFailure() {
                view.getActivity().runOnUiThread(() -> {
                    view.hideProgressBar();
                    view.showGeneralError("No se pudo acceder a su cuenta de Google");
                });
            }
        });
    }

    @Override
    public void onSuccess() {
        SharedPreferences preferences1 = view.getActivity()
                .getSharedPreferences("auth", Context.MODE_PRIVATE);
        preferences1.edit()
                .putBoolean("logged", true)
                .apply();

        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        SharedPreferences preferences2 = view.getActivity()
                .getSharedPreferences("user", Context.MODE_PRIVATE);
        preferences2.edit()
                .putString("emailId", getEmailId(loginInfo.getEmail()))
                .apply();

        view.getActivity().runOnUiThread(() -> {
            view.hideProgressBar();
            view.showSomeActivity();
        });
    }

    @Override
    public void onFailure() {
        view.getActivity().runOnUiThread(() -> {
            view.showGeneralError("Credenciales inválidas");
            view.hideProgressBar();
        });
    }

    private String getEmailId(String email) {
        return email.replace('@', '_').replace('.', '_');
    }
}
