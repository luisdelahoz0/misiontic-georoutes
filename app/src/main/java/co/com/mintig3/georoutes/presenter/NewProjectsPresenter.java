package co.com.mintig3.georoutes.presenter;

import co.com.mintig3.georoutes.model.NewProjectsInteractor;
import co.com.mintig3.georoutes.mpv.NewProjectsMVP;

public class NewProjectsPresenter implements NewProjectsMVP.Presenter {

    private NewProjectsMVP.View view;
    private NewProjectsMVP.Model model;

    public NewProjectsPresenter(NewProjectsMVP.View view){
        this.view = view;
        this.model = new NewProjectsInteractor();
    }

    @Override
    public void onSaveNewClick() {
        boolean error = false;
        NewProjectsMVP.NewProjectsInfo newProjectsInfo = view.getNewProjectsInfo();

        //valida datos
        view.showNameProjectsError("");
        view.showDescriptionProjectsError("");

        if (newProjectsInfo.getNameProject().isEmpty()){
            view.showNameProjectsError("Nombre del proyecto requerido");
            error = true;
        }
        if (newProjectsInfo.getDescriptionProjects().isEmpty()){
            view.showDescriptionProjectsError("Descripción requerida");
            error = true;
        }

        if (!error) {
            // organzare el show al que corresponde
            view.showProjectPoligMenuActivity();
        } else {
            view.showGenerarError("Credenciales Inválidas");
        }



    }
}
