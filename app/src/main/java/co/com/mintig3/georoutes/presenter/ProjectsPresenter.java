package co.com.mintig3.georoutes.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import java.util.List;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.model.ProjectsInteractor;
import co.com.mintig3.georoutes.model.repository.FirebaseAuthRepository;
import co.com.mintig3.georoutes.mpv.ProjectsMVP;

public class ProjectsPresenter implements ProjectsMVP.Presenter {


    private ProjectsMVP.View view;
    private ProjectsMVP.Model model;

    private String user;

    public ProjectsPresenter(ProjectsMVP.View view) {
        SharedPreferences preferences = view.getActivity()
                .getSharedPreferences("user", Context.MODE_PRIVATE);
        this.user = preferences.getString("emailId", null);

        this.view = view;
        this.model = new ProjectsInteractor(view.getActivity(), user);


    }

    public String getUser() {
        return this.user;
    }


    @Override
    public void loadProjects() {
        view.showProgressBar();
        new Thread(() -> {
            model.loadProjects(this.user, new ProjectsMVP.Model.LoadProjectsCallback<List<ProjectsMVP.ProjectDto>>() {

                @Override
                public void onSuccess(List<ProjectsMVP.ProjectDto> projects) {
                    view.getActivity().runOnUiThread(() -> {

                        view.hideProgressBar();
                        view.showProjects(projects);

                    });
                }

                @Override
                public void onFailure() {
                    Log.e(LoginPresenter.class.getSimpleName(), "No hay datos en la base de datos");
                }
            });
        }).start();

    }

    @Override
    public void onProjectsClick() {

    }

    @Override
    public void onBtnNewProjectClick() {
        view.showNewProjectsActivity();
    }


    @Override
    public void onselectItem(ProjectsMVP.ProjectDto item) {
        Bundle params = new Bundle();
        params.putString("project", item.getProject());
        params.putString("address", item.getAddress());

        view.openDetailsActivity(params);
    }

    @Override
    public void logOut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(view.getActivity());

        builder.setTitle(R.string.app_name)
                .setMessage("¿Confirmar cerrar sesión?")
                .setPositiveButton("Cerrar sesión",
                        (dialog, which) -> {
                            SharedPreferences preferences1 = view.getActivity()
                                    .getSharedPreferences("auth", Context.MODE_PRIVATE);
                            preferences1.edit()
                                    .putBoolean("logged", false)
                                    .apply();

                            SharedPreferences preferences2 = view.getActivity()
                                    .getSharedPreferences("user", Context.MODE_PRIVATE);
                            preferences2.edit()
                                    .putString("emailId", null)
                                    .apply();

                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(view.getActivity());
                            repository.logOut();
                            view.getActivity().onBackPressed();
                        })
                .setNegativeButton("No", null);

        builder.create().show();
    }

}
