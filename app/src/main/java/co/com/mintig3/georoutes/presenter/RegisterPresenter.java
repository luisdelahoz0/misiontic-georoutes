package co.com.mintig3.georoutes.presenter;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.model.RegisterInteractor;
import co.com.mintig3.georoutes.model.database.entity.User;
import co.com.mintig3.georoutes.mpv.RegisterMVP;

public class RegisterPresenter implements RegisterMVP.Presenter, RegisterMVP.Model.CreateUserCallback {
    private RegisterMVP.View view;
    private RegisterMVP.Model model;

    public RegisterPresenter(RegisterMVP.View view) {
        this.view = view;
        this.model = new RegisterInteractor(view.getActivity());
    }

    @Override
    public void onRegisterClick() {
        boolean error = false;
        RegisterMVP.RegisterInfo registerInfo = view.getRegisterInfo();

        // Validate data
        view.showNameError("");
        view.showEmailError("");
        view.showPasswordError("");
        view.showConfirmPasswordError("");

        if (registerInfo.getName().isEmpty()) {
            view.showNameError(view.getActivity().getString(R.string.show_name_error));
            error = true;
        } else if (!isNameValid(registerInfo.getName())) {
            view.showNameError(view.getActivity().getString(R.string.show_valid_name_error));
            error = true;
        }

        if (registerInfo.getEmail().isEmpty()) {
            view.showEmailError(view.getActivity().getString(R.string.show_email_error));
            error = true;
        } else if (!isEmailValid(registerInfo.getEmail())) {
            view.showEmailError(view.getActivity().getString(R.string.show_valid_email_error));
            error = true;
        }

        if (registerInfo.getPassword().isEmpty()) {
            view.showPasswordError(view.getActivity().getString(R.string.show_password_is_required));
            error = true;
        } else if (!isPasswordValid(registerInfo.getPassword())) {
            view.showPasswordError(view.getActivity().getString(R.string.show_valid_password_error));
            error = true;
        }

        if (registerInfo.getConfirmedPassword().isEmpty()) {
            view.showConfirmPasswordError(view.getActivity().getString(R.string.show_password_is_required));
            error = true;
        } else if (!doPasswordsMatch(registerInfo.getPassword(), registerInfo.getConfirmedPassword())) {
            view.showPasswordsDontMatch(view.getActivity().getString(R.string.show_do_password_match_error));
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> {
                User user = new User(registerInfo.getName(), registerInfo.getEmail(), registerInfo.getPassword());
                model.createUser(user,
                        RegisterPresenter.this);
            }).start();
        }
    }

    private boolean isNameValid(String password) {
        return password.length() >= 3;
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.endsWith(".com");
    }

    private boolean doPasswordsMatch(String password, String confirmedPassword) {
        return password.equals(confirmedPassword);
    }

    @Override
    public void onSuccess() {
        view.getActivity().runOnUiThread(() -> {
            view.hideProgressBar();
            view.showSomeActivity();
        });
    }

    @Override
    public void onFailure() {
        view.getActivity().runOnUiThread(() -> {
            view.showGeneralError("No se puedo completar el registro");
            view.hideProgressBar();
        });
    }

}
