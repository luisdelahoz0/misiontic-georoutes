package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import co.com.mintig3.georoutes.R;

public class FirstScreen extends AppCompatActivity {

    Button toLog;
    Button toSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);

        toLog = findViewById(R.id.toLogin);
        toLog.setOnClickListener(v ->
                {
                    Intent IntentToLog = new Intent(FirstScreen.this, LoginActivity.class);
                    startActivity(IntentToLog);
                }
        );

        toSignup = findViewById(R.id.toLogin2);

        toSignup.setOnClickListener(v ->
                {
                    Intent intentToSignup = new Intent(FirstScreen.this, register_activity.class);
                    startActivity(intentToSignup);
                }
        );
    }
}
