package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.mpv.ForCoordiMVP;
import co.com.mintig3.georoutes.presenter.ForCoordiPresenter;
import co.com.mintig3.georoutes.view.adapters.ForCoordiAdapter;
import co.com.mintig3.georoutes.view.dto.ForCoordiItem;

public class ForCoordinatesActivity extends AppCompatActivity implements ForCoordiMVP.View {
    private TextInputLayout tilnamePoligono;
    private TextInputEditText etnamePoligono;
    private TextInputLayout tildescription;
    private TextInputEditText etdescription;
    private TextInputLayout tilNewCoodinates;
    private TextInputEditText etNewCoodinates;
    private RecyclerView rvcoordinates;
    private ForCoordiAdapter forCoordiAdapter;

    private FloatingActionButton btnAddCoordinates;


    private ForCoordiMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_coordinates);

        presenter = new ForCoordiPresenter(ForCoordinatesActivity.this);

        initUI();
        presenter.loadCoordinate();
    }

    private void initUI() {
        tilnamePoligono = findViewById(R.id.til_namePolígono);
        etnamePoligono = findViewById(R.id.et_namePolígono);

        tildescription = findViewById(R.id.til_description);
        etdescription = findViewById(R.id.et_description);

        tilNewCoodinates = findViewById(R.id.til_new_coodinates);
        //Cuando el usuario le de click al icono final traigame el metodo addNewCoordinate
        tilNewCoodinates.setEndIconOnClickListener(v -> presenter.addNewCoordinate());
        etNewCoodinates = findViewById(R.id.et_new_coodinates);

        forCoordiAdapter = new ForCoordiAdapter();
        forCoordiAdapter.setLongClickListener(item -> presenter.ForCoordinateItemLongClicked(item));
        //ForCoordinateItemClicked

        rvcoordinates = findViewById(R.id.rv_coordinates);
        rvcoordinates.setLayoutManager(new LinearLayoutManager(ForCoordinatesActivity.this));
        rvcoordinates.setAdapter(forCoordiAdapter);

        //Save Coordinates
        btnAddCoordinates = findViewById(R.id.btn_addCoordinates);
        btnAddCoordinates.setOnClickListener(v -> presenter.onBtnSaveCoordinates());
    }


    @Override
    public ForCoordiMVP.ForCoordinatesInfo getForCoordinatesInfo() {
        return new ForCoordiMVP.ForCoordinatesInfo(
                etnamePoligono.getText().toString().trim(),
                etdescription.getText().toString().trim(),
                etNewCoodinates.getText().toString().trim());
    }

    @Override
    public void showNamePolygonError(String error) {
        tilnamePoligono.setError(error);

    }

    @Override
    public void showDescriptionError(String error) {
        tildescription.setError(error);

    }

    @Override
    public void showAddCoordinatesError(String error) {
        tilNewCoodinates.setError(error);

    }

    @Override
    //Falta organizar vien esta para que siga el orden del Mockups
    public void showPolygonActivity() {
        //Intent intent = new Intent(ForCoordinatesActivity.this, PoligonosActivity.class);
        //startActivity(intent);

    }

    @Override
    public void showGenerarError(String error) {
        Toast.makeText(ForCoordinatesActivity.this,error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCoordinateList(List<ForCoordiItem> items) {
        forCoordiAdapter.setData(items);
    }

    @Override
    public String getCoordinateDescription() {
        return etNewCoodinates.getText().toString();
    }

    @Override
    public void addCoordinateToList(ForCoordiItem coordinate) {
        forCoordiAdapter.addItem(coordinate);
    }

    @Override
    public void showDeleteDialog(String message, ForCoordiItem coordinate) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Coordinate selected")
                .setMessage(message)
                .setPositiveButton("Yes", (dialog, which) -> presenter.deleteCoordinate(coordinate))
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void deleteCoordinate(ForCoordiItem coordinate) {
        forCoordiAdapter.removeTask(coordinate);
    }
}