package co.com.mintig3.georoutes.view;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.mpv.LoginMVP;
import co.com.mintig3.georoutes.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private final int RC_SIGN_IN = 1;
    ActivityResultLauncher<Intent> googleLauncher;

    private CircularProgressIndicator pbWait;

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private AppCompatButton btnLogin;
    private SignInButton btnGoogle;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);
        presenter.validateHasUserAuthenticated();

        initUI();
    }

    private void initUI(){
        pbWait = findViewById(R.id.pb_wait);
        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> presenter.onLoginClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> presenter.onGoogleClick());

        googleLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() ==Activity.RESULT_OK) {
                            Intent data = result.getData();
                            presenter.setGoogleData(data);
                        } else {
                            hideProgressBar();
                        }
                    }
                }
        );
    }


    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showSomeActivity() {
        Intent intent = new Intent(LoginActivity.this, ProjectsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnGoogle.setEnabled(true);
        tilEmail.setEnabled(true);
        tilPassword.setEnabled(true);
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnGoogle.setEnabled(false);
        tilEmail.setEnabled(false);
        tilPassword.setEnabled(false);
    }

    @Override
    public void showGoogleSingInActivity(Intent intent) {
        googleLauncher.launch(intent);
    }
}
