package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.mpv.NewProjectsMVP;
import co.com.mintig3.georoutes.presenter.NewProjectsPresenter;

public class NewProjectsActivity extends AppCompatActivity implements NewProjectsMVP.View {

    private ImageView ivnewProjects;
    private TextInputLayout tilnameProjects;
    private TextInputEditText etnameProjects;
    private TextInputLayout tildescriptionProjects;
    private TextInputEditText etdescriptionProjects;

    private AppCompatButton btnSave;

    private NewProjectsMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_projects);

        presenter = new NewProjectsPresenter(this);

        initUI();
    }

    private void initUI() {

        ivnewProjects = findViewById(R.id.iv_NewProjects);

        tilnameProjects = findViewById(R.id.til_nameProjects);
        etnameProjects = findViewById(R.id.et_nameProjects);

        tildescriptionProjects = findViewById(R.id.til_descriptionProjects);
        etdescriptionProjects = findViewById(R.id.et_descriptionProjects);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> presenter.onSaveNewClick());
    }

    @Override
    public NewProjectsMVP.NewProjectsInfo getNewProjectsInfo() {
        return new NewProjectsMVP.NewProjectsInfo(
                etnameProjects.getText().toString().trim(),
                etdescriptionProjects.getText().toString().trim());
    }

    @Override
    public void showNameProjectsError(String error) {
        tilnameProjects.setError(error);
    }

    @Override
    public void showDescriptionProjectsError(String error) {
        tildescriptionProjects.setError(error);
    }

    @Override
    public void showProjectPoligMenuActivity() {
        Intent intent = new Intent(NewProjectsActivity.this, PoligonosActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGenerarError(String error) {
        Toast.makeText(NewProjectsActivity.this,error, Toast.LENGTH_SHORT).show();
    }
}