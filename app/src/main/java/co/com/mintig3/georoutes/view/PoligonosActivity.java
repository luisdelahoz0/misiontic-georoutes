package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import co.com.mintig3.georoutes.R;

public class PoligonosActivity extends AppCompatActivity {

    FloatingActionButton addPoligon;
    Button toCoo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poligonos);

        addPoligon = findViewById(R.id.addPoligon);
        addPoligon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(
                        PoligonosActivity.this, R.style.BottomSheetDialogTheme
                );
                View bottomSheetView = LayoutInflater.from(getApplicationContext())
                        .inflate(
                                R.layout.bottom_menu_poligons,
                                (LinearLayout)findViewById(R.id.bottomSheetContainer)
                        );
                bottomSheetView.findViewById(R.id.btn_ubicar).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                                {
                                    Intent IntentToCoo= new Intent(PoligonosActivity.this, ForCoordinatesActivity.class);
                                    startActivity(IntentToCoo);
                                }
                    }
                });
                bottomSheetView.findViewById(R.id.btn_coo).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        {
                            Intent IntentToCoo= new Intent(PoligonosActivity.this, CooMap.class);
                            startActivity(IntentToCoo);
                        }
                    }
                });
                bottomSheetDialog.setContentView(bottomSheetView);
                bottomSheetDialog.show();
            }
        });
    }
}