package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.model.repository.FirebaseAuthRepository;
import co.com.mintig3.georoutes.mpv.ProjectsMVP;
import co.com.mintig3.georoutes.presenter.ProjectsPresenter;
import co.com.mintig3.georoutes.view.adapters.ProjectsAdapter;

public class ProjectsActivity extends AppCompatActivity implements ProjectsMVP.View {

    private LinearProgressIndicator pbWait;

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private RecyclerView rvProjects;
    //private FloatingActionButton btnNewProject;
    private ExtendedFloatingActionButton btnNewProject;

    private ProjectsMVP.Presenter presenter;
    private ProjectsAdapter projectsAdapter;

    private TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        presenter = new ProjectsPresenter(ProjectsActivity.this);

        initUI();
        presenter.loadProjects();
        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(ProjectsActivity.this);
        FirebaseUser user = repository.getCurrentUser();
        tvEmail.setText(user.getEmail());
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(navigationView));

        pbWait = findViewById(R.id.pb_wait);


        navigationView = findViewById(R.id.nv_projects);
        navigationView.setNavigationItemSelectedListener(menuItem -> onMenuItemClick(menuItem));

        rvProjects = findViewById(R.id.rv_projects);
        rvProjects.setLayoutManager(new LinearLayoutManager(ProjectsActivity.this));

        projectsAdapter = new ProjectsAdapter();
        projectsAdapter.setListener(new ProjectsAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(ProjectsMVP.ProjectDto item) {
                presenter.onselectItem(item);
            }
        });
        rvProjects.setAdapter(projectsAdapter);

        tvEmail = navigationView.getHeaderView(0).findViewById(R.id.tv_email);

        //Para el boton de new project
        btnNewProject = findViewById(R.id.btn_extend_newproject);
        btnNewProject.setOnClickListener(v -> presenter.onBtnNewProjectClick());
    }

    private boolean onMenuItemClick(MenuItem menuItem){
        menuItem.setChecked(true);
        presenter.logOut();//TODO
        drawerLayout.closeDrawers();
        return true;
    }


    @Override
    public Activity getActivity() {
        return ProjectsActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);

    }

    @Override
    public void showProjects(List<ProjectsMVP.ProjectDto> projects) {
        projectsAdapter.setData(projects);

    }


    @Override
    //Activity Details projects,edit
    //T conexion a activitidad detalles de projecto
    public void openDetailsActivity(Bundle params) {
        Intent intent = new Intent(ProjectsActivity.this, LoginActivity.class);
        intent.putExtras(params);
        //startActivity(intent);
        Toast.makeText(ProjectsActivity.this, params.getString("project"), Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void showNewProjectsActivity() {
        Intent intent = new Intent(ProjectsActivity.this, NewProjectsActivity.class);
        startActivity(intent);
    }

}
