package co.com.mintig3.georoutes.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.view.dto.ForCoordiItem;
import co.com.mintig3.georoutes.view.dto.ForCoordiState;

public class ForCoordiAdapter extends RecyclerView.Adapter<ForCoordiAdapter.ViewHolder> {
    private List<ForCoordiItem> data;
    private OnItemClickListener listener;
    private OnItemClickListener longListener;

    public ForCoordiAdapter() {
        data = new ArrayList<>();
    }

    public void setData(List<ForCoordiItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addItem(ForCoordiItem item) {
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }


    public void setLongClickListener(OnItemClickListener listener) {
        this.longListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_for_coordinates, parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ForCoordiItem item = data.get(position);

        if (listener != null) {
            holder.itemView.setOnClickListener(v -> listener.onClick(item));
        }

        if (longListener != null) {
            holder.itemView.setOnLongClickListener(v -> {
                longListener.onClick(item);
                return false;
            });
        }

        holder.tvDescription.setText(item.getDescription());
        holder.tvDate.setText(item.getDate());
        int color = item.getState() == ForCoordiState.SAVE ? R.color.save_coordinate
                : R.color.delete_coordinate;

        holder.ivIcon.setColorFilter(
                ContextCompat.getColor(holder.itemView.getContext(), color),
                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }
    public void removeTask(ForCoordiItem coordinate) {
        int i = data.indexOf(coordinate);
        data.remove(i);
        notifyItemRemoved(i);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView ivIcon;
        private final TextView tvDescription;
        private final TextView tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }

    public interface OnItemClickListener {
        void onClick(ForCoordiItem item);
    }
}