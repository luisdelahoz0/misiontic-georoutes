package co.com.mintig3.georoutes.view.adapters;

import android.content.ClipData;
import android.location.GnssAntennaInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.mpv.ProjectsMVP;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ViewHolder> {

    private List<ProjectsMVP.ProjectDto> data;
    private OnItemClickListener listener;

    public ProjectsAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<ProjectsMVP.ProjectDto> data){
        this.data = data;
        //notifyDataSetChanged();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_projects, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProjectsMVP.ProjectDto item = data.get(position);

        if (listener != null) {
            holder.itemView.setOnClickListener(v -> {
                listener.onItemClicked(item);
            });
        }

        holder.getTvProjects().setText(item.getProject());
        holder.getTvAddress().setText(item.getAddress());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivProjects;
        private TextView tvProjects;
        private TextView tvAddress;
        public ViewHolder(View view) {
            super(view);


            initUI(view);
            
        }

        private void initUI(View view) {
            ivProjects = view.findViewById(R.id.iv_project);
            tvProjects =  view.findViewById(R.id.tv_name_project);
            tvAddress =  view.findViewById(R.id.tv_address);
        }

        public ImageView getIvProjects() {
            return ivProjects;
        }
        public TextView getTvProjects() {
            return tvProjects;
        }
        public TextView getTvAddress() {
            return tvAddress;
        }
    }

    public static interface OnItemClickListener {
        void onItemClicked(ProjectsMVP.ProjectDto item);
    }
}
