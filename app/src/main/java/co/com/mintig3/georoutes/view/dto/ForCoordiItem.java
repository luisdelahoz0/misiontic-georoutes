package co.com.mintig3.georoutes.view.dto;

import java.util.Objects;

public class ForCoordiItem {
    private String description;
    private String date;
    private ForCoordiState state;

    public ForCoordiItem(String description, String date) {
        this.description = description;
        this.date = date;
        this.state = ForCoordiState.SAVE;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public ForCoordiState getState() {
        return state;
    }

    public void setState(ForCoordiState state) {
        this.state = state;
    }
}
