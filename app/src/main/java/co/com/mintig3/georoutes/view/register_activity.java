package co.com.mintig3.georoutes.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.mintig3.georoutes.R;
import co.com.mintig3.georoutes.mpv.LoginMVP;
import co.com.mintig3.georoutes.mpv.RegisterMVP;
import co.com.mintig3.georoutes.presenter.LoginPresenter;
import co.com.mintig3.georoutes.presenter.RegisterPresenter;

public class register_activity extends AppCompatActivity implements RegisterMVP.View {

    private LinearProgressIndicator pbWait;

    private TextInputLayout tilName;
    private TextInputEditText etName;

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private TextInputLayout tilConfirmPassword;
    private TextInputEditText etConfirmPassword;
    private AppCompatButton btnRegister;

    private RegisterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);

        initUI();
    }

    private void initUI(){
        pbWait = findViewById(R.id.pb_wait);

        tilName = findViewById(R.id.til_name);
        etName = findViewById(R.id.et_name);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        tilConfirmPassword = findViewById(R.id.til_confirm_password);
        etConfirmPassword = findViewById(R.id.et_confirm_password);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterClick());

    }

    @Override
    public Activity getActivity() {
        return register_activity.this;
    }

    @Override
    public RegisterMVP.RegisterInfo getRegisterInfo() {
        return new RegisterMVP.RegisterInfo(
                etName.getText().toString().trim(),
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim(),
                etConfirmPassword.getText().toString().trim());
    }

    @Override
    public void showNameError(String error) {
        tilName.setError(error);
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showConfirmPasswordError(String error) {
        tilConfirmPassword.setError(error);
    }

    @Override
    public void showPasswordsDontMatch(String error) {
        tilPassword.setError(error);
        tilConfirmPassword.setError(error);
    }

    @Override
    public void showSomeActivity() {
        Intent intent = new Intent(register_activity.this, ProjectsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(register_activity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnRegister.setEnabled(true);
        tilName.setEnabled(true);
        tilEmail.setEnabled(true);
        tilPassword.setEnabled(true);
        tilConfirmPassword.setEnabled(true);
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnRegister.setEnabled(false);
        tilName.setEnabled(false);
        tilEmail.setEnabled(false);
        tilPassword.setEnabled(false);
        tilConfirmPassword.setEnabled(false);
    }
}
